package com.pason.contacts;

import java.awt.*;
import javax.swing.*;

public class App {

    public static void main(String[] args) {
	    SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGui();
            }
        });
    }
	
    private static void createAndShowGui() {
        ContactPanel panel = new ContactPanel();

        JFrame frame = new JFrame("Contacts");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
    }
}