package com.pason.contacts;

import java.awt.*;
import javax.swing.*;

public class ContactPanel extends JPanel {

    private JLabel firstNameLbl = new JLabel("First Name:");
    private JLabel lastNameLbl = new JLabel("Last Name:");
    private JLabel phoneNumberLbl = new JLabel("Phone Number:");
    private JTextField firstNameTf = new JTextField();
    private JTextField lastNameTf = new JTextField();
    private JTextField phoneNumberTf = new JTextField();

    public ContactPanel() {
        setLayout(new GridLayout(3, 3));
        add(firstNameLbl);
        add(firstNameTf);
        add(lastNameLbl);
        add(lastNameTf);
        add(phoneNumberLbl);
        add(phoneNumberTf);
    }
	
    public String getFirstName() {
        return firstNameTf.getText();
    }
	
    public String getLastName() {
        return lastNameTf.getText();
    }
	
    public String getPhoneNumber() {
        return phoneNumberTf.getText();
    }
}